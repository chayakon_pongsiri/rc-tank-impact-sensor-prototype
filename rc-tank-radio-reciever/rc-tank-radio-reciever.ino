#include <Wire.h>
//#include <Thread.h>
//#include <ThreadController.h>

#include <PinChangeInterrupt.h>
#include <TimerOne.h>

#define PIN_COUNT 6    //number of channels attached to the receiver
#define MAX_PIN_CHANGE_PINS PIN_COUNT

#define CH1_PIN 7
#define CH2_PIN 6
#define CH3_PIN 5
#define CH4_PIN 4
#define CH5_PIN 3
#define CH6_PIN 2

byte pin[] = {7, 6, 5, 4, 3, 2};
int time[] = {0,0,0,0,0,0};
byte state=0;
byte i=0;

String SEPARATOR = "|";
volatile int ch1,ch2,ch3,ch4,ch5,ch6;

//ThreadController control = ThreadController();
//Thread* serialThread = new Thread();

void setup() {
  Serial.begin(9600);  
  Serial.println("Slave sender node");

  setupRcReciever();
  /*
  serialThread->onRun(serialThreadCallback);
  serialThread->setInterval(100);
  control.add(serialThread);
  */
  Wire.begin(1); 
  Wire.onRequest(sendEventData);
}

void setupRcReciever() {
  Timer1.initialize(2200);
  Timer1.stop();
  Timer1.restart();
  
  for (byte i=0; i < MAX_PIN_CHANGE_PINS; i++)
  {
      pinMode(pin[i], INPUT);     //set the pin to input
      digitalWrite(pin[i], HIGH); //use the internal pullup resistor
  }
  attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(pin[i]), rise, RISING);
}

void loop() {
  //control.run();
  switch (state)
  {
    case RISING: //we have just seen a rising edge
      detachPinChangeInterrupt(digitalPinToPinChangeInterrupt(pin[i]));
      attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(pin[i]), fall, FALLING); //attach the falling end
      state=255;
      break;
    case FALLING: //we just saw a falling edge
      detachPinChangeInterrupt(digitalPinToPinChangeInterrupt(pin[i]));
      i++;                //move to the next pin
      i = i % PIN_COUNT;  //i ranges from 0 to PIN_COUNT
      attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(pin[i]), rise,RISING);
      state=255;
      break;
  }
}

void serialThreadCallback() {
  char buffer[50];
  sprintf(buffer, "%d:%d:%d:%d:%d:%d\n", time[0], time[1], time[2], time[3], time[4], time[5]);
  Serial.print(buffer);
}

void sendEventData() {
  char buffer[50];
  sprintf(buffer, "%d:%d:%d:%d:%d:%d\n", time[0], time[1], time[2], time[3], time[4], time[5]);
  Wire.print(buffer);
}

void rise()        //on the rising edge of the currently interesting pin
{
  Timer1.restart();        //set our stopwatch to 0
  Timer1.start();            //and start it up
  state=RISING;
}
 
void fall()        //on the falling edge of the signal
{
  state=FALLING;
  time[i]=readTimer1();    // read the time since timer1 was restarted
  Timer1.stop();
}
 
unsigned long readTimer1()        //returns the value of the timer in microseconds
{                                    //remember! phase and freq correct mode counts
                                    //up to ICR1 then down again
    unsigned int tmp=TCNT1;
    char scale=0;
    while (TCNT1==tmp) //if the timer has not ticked yet
    {
        //do nothing -- max delay here is ~1023 cycles
    }
    tmp = (  (TCNT1>tmp) ? (tmp) : (ICR1-TCNT1)+ICR1  );//if we are counting down add the top value
                                                        //to how far we have counted down
    return ((tmp*1000L)/(F_CPU /1000L))<<scale;
}

