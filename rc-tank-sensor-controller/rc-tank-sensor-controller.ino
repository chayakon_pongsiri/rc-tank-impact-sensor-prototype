#include <Thread.h>
#include <ThreadController.h>
#include <Wire.h>
#include <MPU6050.h>
#include <Servo.h>
//#include <Arduino.h>
//#include <avr/wdt.h>

//Turret elevation servo
#define MIN_SERVO 15
#define MAX_SERVO 150
#define SERVO_PIN 10
#define SERVO_START_POS 90
#define SLAVE_ADDRESS 1   //I2C to arduino address
#define PAYLOAD_SIZE 50   //I2C data size
#define BAUD_RATE 9600

//Turret turn motor
//#define TURRET_TURN_MOTOR_L 6
//#define TURRET_TURN_MOTOR_R 9


ThreadController control = ThreadController();
Thread* mpu6050Thread = new Thread();
Thread* rcThread = new Thread();
Thread* serialThread = new Thread();

Servo servo;
MPU6050 mpu;

float timeStep = 0.01;

// Pitch, Roll and Yaw values
float pitch = 0, roll = 0, yaw = 0;
int iaccpitch = 0, ipitch = 0, iroll = 0, iyaw = 0;
int servoPos;

int startYaw = 0;
int startPitch = 0;

String data = "";
Vector norm;
Vector normAccel;

void setup() {
  //wdt_disable();
  
  Serial.begin(BAUD_RATE);  
  Serial.println("|OUT=Start"); 
  Serial.println("|ON=0");

  setupServo();
  setupMpu6050();
  
  // config thread
  mpu6050Thread->onRun(mpu6050ThreadCallback);
  mpu6050Thread->setInterval(10);

  rcThread->onRun(rcThreadCallback);
  rcThread->setInterval(50);
  
  serialThread->onRun(serialThreadCallback);
  serialThread->setInterval(50);

  control.add(mpu6050Thread);
  control.add(rcThread);
  control.add(serialThread);
  
  Wire.begin();

  //enable watchdog
  //wdt_enable(WDTO_2S);
}

void loop() {
  //wdt_reset();
  control.run();
}

void setupServo() {
  //Init servo on port A0
  servo.attach(SERVO_PIN);
  servoPos = SERVO_START_POS;
  servo.write(servoPos);
  delay(2000);
}

void setupMpu6050() {
  // Initialize MPU6050
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  {
    Serial.println("DEBUG=Could not find a valid MPU6050 sensor");
    delay(500);
  }
  mpu.calibrateGyro();
  mpu.setThreshold(1);
  
  norm = mpu.readNormalizeGyro();
  normAccel = mpu.readNormalizeAccel();
  for (int i = 0; i < 10; i++) {
    norm = mpu.readNormalizeGyro();
    normAccel = mpu.readNormalizeAccel();
    iaccpitch = -(atan2(normAccel.XAxis, sqrt(normAccel.YAxis*normAccel.YAxis + normAccel.ZAxis*normAccel.ZAxis))*180.0)/M_PI;
  }
  startPitch = iaccpitch;
}

void mpu6050ThreadCallback() 
{ 
  // Read normalized values
  norm = mpu.readNormalizeGyro();
  normAccel = mpu.readNormalizeAccel();

  // Calculate Pitch, Roll and Yaw from gyro
  pitch = pitch + norm.YAxis * timeStep;
  roll = roll + norm.XAxis * timeStep;
  yaw = yaw + norm.ZAxis * timeStep;

  iaccpitch = -(atan2(normAccel.XAxis, sqrt(normAccel.YAxis*normAccel.YAxis + normAccel.ZAxis*normAccel.ZAxis))*180.0)/M_PI;

  ipitch = (int)(pitch * 10);
  iroll = (int)(roll * 10);
  iyaw = (int)(yaw * 10);

  updateTurretServo();
  //updateTurretTurnMotor();
}

/*
void updateTurretTurnMotor() {
  int diff = ipitch;
  if (diff > 350) {
    analogWrite(TURRET_TURN_MOTOR_L, 150);
    analogWrite(TURRET_TURN_MOTOR_R, 0);
  } else if (diff < -350) {
    analogWrite(TURRET_TURN_MOTOR_L, 0);
    analogWrite(TURRET_TURN_MOTOR_R, 150);
  if (diff > 150) {
    analogWrite(TURRET_TURN_MOTOR_L, 60);
    analogWrite(TURRET_TURN_MOTOR_R, 0);
  } else if (diff < -150) {
    analogWrite(TURRET_TURN_MOTOR_L, 0);
    analogWrite(TURRET_TURN_MOTOR_R, 60);
  } else if (diff > 50) {
    analogWrite(TURRET_TURN_MOTOR_L, 30);
    analogWrite(TURRET_TURN_MOTOR_R, 0);
  } else if (diff < -50) {
    analogWrite(TURRET_TURN_MOTOR_L, 0);
    analogWrite(TURRET_TURN_MOTOR_R, 30);
  } else if (diff > 30) {
    analogWrite(TURRET_TURN_MOTOR_L, 20);
    analogWrite(TURRET_TURN_MOTOR_R, 0);
  } else if (diff < -30) {
    analogWrite(TURRET_TURN_MOTOR_L, 0);
    analogWrite(TURRET_TURN_MOTOR_R, 20);
  } else {
    analogWrite(TURRET_TURN_MOTOR_L, 0);
    analogWrite(TURRET_TURN_MOTOR_R, 0);
  }
}*/

void updateTurretServo() {
  int diff =  startPitch - iaccpitch;
  if (diff > 10) {
     servoPos += 3;
  } else if (diff < -10) {
    servoPos -= 3;
  } else if (diff > 5) {
     servoPos += 2;
  } else if (diff < -5) {
    servoPos -= 2;
  } else if (diff > 2) {
    servoPos += 1;
  } else if (diff < -2) {
    servoPos -= 1;
  }
  
  if (servoPos > MAX_SERVO) {
    servoPos = MAX_SERVO;
  } else if (servoPos < MIN_SERVO) {
    servoPos = MIN_SERVO;
  }
  servo.write(servoPos);
}

void rcThreadCallback() {
  Wire.requestFrom(SLAVE_ADDRESS, PAYLOAD_SIZE);
  data = "";
  while (Wire.available()) {
    char c = Wire.read();
    if (c == '\n' || c == '\r') {
      break;
    }
    data += c;
  }
}

void serialThreadCallback() 
{
  //wdt_reset();
  String response = "";
  while(Serial.available() > 0) {
    char command = Serial.read();
    if (command == 10 || command == 13) {
      break;
    }
    int value = Serial.read();
    if (command == 'u') {
      // Turret up
      startPitch += 1;
      response = "turret_up_" + startPitch;
      break;
    } else if (command == 'd') {
      // Turret down
      startPitch -= 1;
      response = "turret_down_" + startPitch;
      break;
    } else if (command == 'r') {
      software_Reset();
      response = "reset";
      break;
    }
  }

  // Response
  Serial.print("|ON=1");
  Serial.print("|AP=");
  Serial.print(iaccpitch);
  Serial.print("|P=");
  Serial.print(ipitch);
  Serial.print("|R=");
  Serial.print(iroll);
  Serial.print("|Y=");
  Serial.print(iyaw);
  Serial.print("|RC=");
  Serial.print(data);
  Serial.print("|OUT=");
  Serial.print(response);
  Serial.print("|");
  Serial.println();
}

void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
  asm volatile ("  jmp 0");  
} 
