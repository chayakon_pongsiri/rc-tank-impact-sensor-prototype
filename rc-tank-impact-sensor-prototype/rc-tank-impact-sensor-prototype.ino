
const int ledPin = 2;      // led connected to digital pin 2
const int knockSensor = A3; // the piezo is connected to analog pin A0
const int threshold = 5;  // threshold value to decide when the detected sound is a knock or not


// these variables will change:
int sensorReading = 0;      // variable to store the value read from the sensor pin
int ledState = LOW;         // variable used to store the last LED status, to toggle the light

void setup() {
  pinMode(ledPin, OUTPUT); // declare the ledPin as as OUTPUT
  Serial.begin(9600);       // use the serial port
}

void loop() {
  // read the sensor and store it in the variable sensorReading:
  sensorReading = analogRead(knockSensor);
  //
  // if the sensor reading is greater than the threshold:
  if (sensorReading >= threshold) {
    // update the LED pin itself:
    Serial.print(sensorReading);
    digitalWrite(ledPin, HIGH);
    Serial.println("Knock!");
    delay(100);
    digitalWrite(ledPin, LOW);
  }
  //delay(1);
}

