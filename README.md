Welcome
- This is a simple prototype for Rc Tank BB Gun impact detection.

 
Hardware
- Arduino Nano
- Piezo Buzzer
- Resistor 1M, 1K

Sketch
![Alt text](images/Impact_Sketch_bb.png)
